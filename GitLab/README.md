Files in this directory are copied from the GitLab official templates.  
They are copied to allow easier overriding of specific variables and tags, while
the code is basically identical to the templates provided by gitlab.
