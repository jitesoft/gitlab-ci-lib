#!/bin/bash

if [ -z ${1+x} ]; then
  echo "GenArch"
  echo "Generate architectural specific gitlab-ci templates for"
  echo "registry.gitlab.com/jitesoft/dockerfiles/misc/buildbase"
  echo ""
  echo "Usage: ./genarch.sh <commit-hash> <version1> <version2...>"
  echo ""
  echo "Example:"
  echo "./genarch.sh ef5c71bc 3.14 3.15 3.16 > archs.yml"
  echo ""
  echo "Requirements:"
  echo "  * docker"
  echo "  * buildx"
  echo "  * jq"
  echo "  * bash"
  echo ""
  echo "Alternative shells *may* work by changing shebang"
  sleep 10
  exit 0
fi

HASH=$1
shift

for version in "$@"; do
  DATA="$(docker buildx imagetools inspect --raw "registry.gitlab.com/jitesoft/dockerfiles/misc/buildbase/$version:$HASH" \
    | jq ".manifests[] | { digest: .digest, arch: .platform.architecture, version: $version }")$DATA"
done

DATA=$(echo "$DATA" | jq -s)
echo "$DATA" | jq -r '.[]|[.arch, .digest, .version] | @tsv' |
while IFS=$'\t' read -r arch digest version; do

  if [ $arch != 'unknown' ]; then
    echo ".buildbase.$version.$arch:"
    echo "  image: registry.gitlab.com/jitesoft/dockerfiles/misc/buildbase/$version:$HASH@$digest"
    echo "  variables:"
    echo "    BUILDBASE_ARCH: \"$arch\""
    echo "    ALPINE_VERSION: \"$version\""
  fi
done

echo ""
echo ".buildbase_variables: "
echo "  variables:"
echo "$DATA" | jq -r '.[]|[.arch, .digest, .version] | @tsv' |
while IFS=$'\t' read -r arch digest version; do
    if [ $arch != 'unknown' ]; then
      VALUE="BUILDBASE_IMAGE_${arch}_${version}: \"registry.gitlab.com/jitesoft/dockerfiles/misc/buildbase/${version}:${HASH}@${digest}\""
      echo "    ${VALUE/./_}"
    fi
done
