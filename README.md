# Gitlab CI Snippets!

To include one of the files in this repo, use the `include` parameter in your .gitlab-ci.yml file.  
All the jobs are stored as hidden jobs, that is, they will not be used even if you include the file, but have to be either included as a anchor or as a extended job.

```yml
include: 
    - 'https://gitlab.com/jitesoft/gitlab-ci-lib/raw/master/file.yml'
# Depending on if you wish to use one of the jobs as a extend or override you can do either:

my_job:
    <<: *job_from_file
    overidden_something: [...]

# Or:

my_job:
    extends: .job_from_file
```

## Why?

Most of the templates for scanning supplied by gitlab are either using docker to run the scans or
uses a local database that need to re-seed every scan. So I created these templates so that
one could skip the docker part (mostly) and also use external databases for some of the jobs (see trivy, clair etc).

## License

All snippets in this repository are licensed under the MIT license.  
Make sure that you check the license for each of the tools that the snippets make use of so that it
complies with your licensing model.

```
MIT License

Copyright (c) 2020 Jitesoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

## Notice

If your scripts stopped working, you might have to point them to the `old` branch of this repository.
