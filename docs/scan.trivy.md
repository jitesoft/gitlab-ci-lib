# Container scanning with local or remote Trivy.

_Jobs can be found [here](https://gitlab.com/jitesoft/gitlab-ci-lib/-/blob/master/Scan/trivy.yml)._

CI configurations which uses [Trivy](https://github.com/aquasecurity/trivy) to scan for vulnerabilities in OCI images.  
The default stage used in the Trivy scripts is named `scan` and can easily be changed if overloading the script with `extends`.


The following environment variables are set with default values in the configurations and can be changed when overriding.  
Be sure to change _all_ of them if changing any, due to variable overriding rules in GitLab CI.

```shell
# Name=Default value
SCANNING_IMAGE_NAME="${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}"
TRIVY_CACHE_DIR="${CI_PROJECT_DIR}/.trivycache"
TRIVY_TEMPLATE="/contrib/gitlab.tpl"
GIT_STRATEGY=none

# Optional
NO_OUTPUT="" # Not set by default
# On remote scanning
TRIVY_ENDPOINT="" # No default
```

`.scan.container.trivy.local` uses Trivy as a local scanner of images, that is, by downloading the database before scanning the image,
while the `.scan.container.trivy.remote` is intended to be used for remote scanning.

Simple overrides of either could look something like the following:

```yaml
include:
  - https://gitlab.com/jitesoft/gitlab-ci-lib/raw/master/Scan/trivy.yml

auto.scan.container.remote.trivy:
  extends: .scan.container.trivy.remote
  rules:
    - if: $NO_AUTO
      when: never
    - if: $CONTAINER_SCANNING_DISABLED
      when: never
    - if: "$SCANNING_IMAGE_NAME && $TRIVY_ENDPOINT"
      when: always
    - when: never

auto.scan.container.local.trivy:
  extends: .scan.container.trivy.local
  rules:
    - if: $NO_AUTO
      when: never
    - if: $CONTAINER_SCANNING_DISABLED
      when: never
    - if: "$SCANNING_IMAGE_NAME && $TRIVY_ENDPOINT"
      when: always
    - when: never
```

The configurations only uses the `script` clause and will automatically create a `gl-container-scanning-report.json` file and
upload it as an artifact report.

# Dependency scanning with local Trivy.

_Job can be found [here](https://gitlab.com/jitesoft/gitlab-ci-lib/-/blob/master/Scan/dependency.yml#L49)._

Trivy can be used as a file-system scanner which scans for package files of multiple types.  

The dependency scan job uses a local Trivy scan (which always downloads the database and caches it). Further, it uses a simple dependency scanning template
which can be found [here](https://gitlab.com/jitesoft/gitlab-ci-lib/-/raw/master/templates/gitlab-dependency-scanning.tpl), which complies with the GitLab dependency
scanning template.

The following environment variables are set with default values in the configurations and can be changed when overriding.  
Be sure to change _all_ of them if changing any, due to variable overriding rules in GitLab CI.

```shell
# Name=Default value
TRIVY_CACHE_DIR="${CI_PROJECT_DIR}/.trivycache"
TRIVY_VULN_TYPE="library"
```

Simple overrides of either could look something like the following:

```yaml
include:
- https://gitlab.com/jitesoft/gitlab-ci-lib/raw/master/Scan/dependency.yml
    - 
auto.scan.trivy:
  extends: .scan.pkg.trivy
  allow_failure: true
  rules:
    - if: $NO_AUTO
      when: never
    - if: $DEPENDENCY_SCANNING_DISABLED
      when: never
    - if: '$CI_COMMIT_BRANCH || $CI_PIPELINE_SOURCE == "external_pull_request_event" || $CI_PIPELINE_SOURCE == "merge_request_event"'
      exists:
        - '{composer.lock,*/composer.lock,*/*/composer.lock}'
        - '{poetry.lock,*/poetry.lock,*/*/poetry.lock}'
        - '{yarn.lock,*/yarn.lock,*/*/yarn.lock}'
        - '{Cargo.lock,*/Cargo.lock,*/*/Cargo.lock}'
        - '{packages.lock.json,*/packages.lock.json,*/*/packages.lock.json}'
        - '{package-lock.json,*/package-lock.json,*/*/package-lock.json}'
        - '{Gemfile.lock,*/Gemfile.lock,*/*/Gemfile.lock}'
        - '{Pipfile.lock,*/Pipfile.lock,*/*/Pipfile.lock}'
        - '{*.jar,*/*.jar,*/*/*.jar}'
        - '{*.war,*/*.war,*/*/*.war}'
        - '{*.ear,*/*.ear,*/*/*.ear}'
      when: always
    - when: never
```

The configurations only uses the `script` clause and will automatically create a `gl-dependency-scanning-report.json` file and
upload it as an artifact report.
