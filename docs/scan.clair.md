# Container scanning with a remote Clair server.

This script is an adaption of the GitLab CI Container scanning script.
It uses a converter script from https://gist.githubusercontent.com/Johannestegner/cedb46c87780ffbbd7f3fbbd86add1d4/raw/GitlabConvert.py
to convert the output to the expected output for gitlab ci container scan.

The script uses a `scan` stage, this can be changed if wanted.

The following variables have been exposed to your disposal:

```
CLAIR_URI           - URI where the clair server is running.
SCANNING_IMAGE_NAME - Name of the image to scan, should be pushed and available for the clair server - Defaults to ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}
CONFIG              - Config file as a file path.

// Extra.
CONTAINER_SCANNING_DISABLED - If this variable is set, the container scanning will not run.
```

If the `CLAIR_URI` is not set, the `CONFIG` file _should_ be set to not break the invocation of clair.  

---

The easiest way to set this up is to either add your own configuration file as a `File` variable in gitlab ci and use `CONFIG` as name.  
If this is not done, the script will try to generate a new configuration using the `CLAIR_URI` and gitlab ci variables for authentication to the
registry. The template used to generate this file can be found [here](https://gist.githubusercontent.com/Johannestegner/556ed344d6fa5314fac0f0e68ea4c822/raw/847f41128eb936abcd9b5faf135f52d495cbdecc/ClairConfig.yml).

